#!/bin/bash
# BookStack backup & update script
# Forked from codemicro: https://gist.github.com/codemicro/f9dc94ecf5831772e1bff92dfba337d0

########### CONFIG ###########

# Configure this to be the location where BookStack is installed.
# Do not include a trailing slash
BOOKSTACK_DIR="/var/www/bookstack"

# Configure this to be the location where you want to store backups.
# Do not include a trailing slash
BACKUP_DIR="/var/www/bookstack/backups"

########### CONFIG END ###########

# Enter BookStack dir
cd $BOOKSTACK_DIR || exit

# Autoload BookStack .env options into current environment
if [ -f .env ]
then
  export $(cat .env | grep ^DB_ | sed 's/#.*//g' | xargs)
else
  exit
fi

DATE=$(date +%Y-%m-%d_%H-%M)
BACKUP_ZIP_NAME="$BACKUP_DIR/$DATE.tar.gz"
BACKUP_TEMP_DB_FILE="$BACKUP_DIR/database.sql"

# Create backup dir if it does not exist
mkdir -p $BACKUP_DIR

# Use loaded .env variables to create DB dump
mysqldump --no-tablespaces -u $DB_USERNAME --password="$DB_PASSWORD" $DB_DATABASE > "$BACKUP_TEMP_DB_FILE"

# Backup BookStack storage files
tar -czvf "$BACKUP_ZIP_NAME" .env public/uploads storage/uploads themes -C "$BACKUP_DIR" database.sql

# Remove the temporary DB file
rm -f "$BACKUP_TEMP_DB_FILE"

# Run BookStack upgrade commands
git pull origin release
composer install --no-dev
php artisan migrate
php artisan cache:clear
php artisan view:clear
